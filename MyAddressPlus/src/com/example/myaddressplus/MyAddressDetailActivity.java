package com.example.myaddressplus;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


/*
 * addressDetailActivity allows to enter a new address item 
 * or to change an existing
 */
public class MyAddressDetailActivity extends Activity {
  private Spinner mDesignation;
  private EditText mFirstNameText;
  private EditText mLastNameText;
  private EditText mAddressText;
  private EditText mCountryText;
  private EditText mPostcodeText;
  private Spinner mProvince;

  private Uri addressUri;

  @Override
  protected void onCreate(Bundle bundle) {
    super.onCreate(bundle);
    setContentView(R.layout.myaddress_edit);

    mDesignation = (Spinner) findViewById(R.id.designation);
    mFirstNameText = (EditText) findViewById(R.id.address_edit_firstname);
    mLastNameText = (EditText) findViewById(R.id.address_edit_lastname);
    mAddressText = (EditText) findViewById(R.id.address_edit_address);
    mCountryText = (EditText) findViewById(R.id.address_edit_country);
    mPostcodeText = (EditText) findViewById(R.id.address_edit_postcode);
    mProvince = (Spinner) findViewById(R.id.provinces);
    Button confirmButton = (Button) findViewById(R.id.address_edit_button);

    Bundle extras = getIntent().getExtras();

    // Check from the saved Instance
    addressUri = (bundle == null) ? null : (Uri) bundle.getParcelable(MyAddressContentProvider.CONTENT_ITEM_TYPE);

    // Or passed from the other activity
    if (extras != null) {
      addressUri = extras.getParcelable(MyAddressContentProvider.CONTENT_ITEM_TYPE);
      fillData(addressUri);
    }

    confirmButton.setOnClickListener(new View.OnClickListener() {
      public void onClick(View view) {
        if (TextUtils.isEmpty(mFirstNameText.getText().toString())) {
          makeToast();
        } else {
          setResult(RESULT_OK);
          finish();
        }
      }

    });
  }

  private void fillData(Uri uri) {
    String[] address = { MyAddressTableHandler.COLUMN_POSTCODE, MyAddressTableHandler.COLUMN_COUNTRY, MyAddressTableHandler.COLUMN_PROVINE, MyAddressTableHandler.COLUMN_ADDRESS, MyAddressTableHandler.COLUMN_LASTNAME, MyAddressTableHandler.COLUMN_FIRSTNAME, MyAddressTableHandler.COLUMN_DESIGNATION };
    Cursor cursor = getContentResolver().query(uri, address, null, null, null);
    if (cursor != null) {
      cursor.moveToFirst();
      String designation = cursor.getString(cursor.getColumnIndexOrThrow(MyAddressTableHandler.COLUMN_DESIGNATION));
      String province = cursor.getString(cursor.getColumnIndexOrThrow(MyAddressTableHandler.COLUMN_PROVINE));
      for (int i = 0; i < mDesignation.getCount(); i++) {

        String s = (String) mDesignation.getItemAtPosition(i);
        if (s.equalsIgnoreCase(designation)) {
          mDesignation.setSelection(i);
        }
      }
      
      for (int i = 0; i < mProvince.getCount(); i++) {

          String p = (String) mProvince.getItemAtPosition(i);
          if (p.equalsIgnoreCase(province)) {
            mProvince.setSelection(i);
          }
      }

      mFirstNameText.setText(cursor.getString(cursor.getColumnIndexOrThrow(MyAddressTableHandler.COLUMN_FIRSTNAME)));
      mLastNameText.setText(cursor.getString(cursor.getColumnIndexOrThrow(MyAddressTableHandler.COLUMN_LASTNAME)));
      mAddressText.setText(cursor.getString(cursor.getColumnIndexOrThrow(MyAddressTableHandler.COLUMN_ADDRESS)));
      mCountryText.setText(cursor.getString(cursor.getColumnIndexOrThrow(MyAddressTableHandler.COLUMN_COUNTRY)));
      mPostcodeText.setText(cursor.getString(cursor.getColumnIndexOrThrow(MyAddressTableHandler.COLUMN_POSTCODE)));

      // Always close the cursor
      cursor.close();
    }
  }

  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    saveState();
    outState.putParcelable(MyAddressContentProvider.CONTENT_ITEM_TYPE, addressUri);
  }

  @Override
  protected void onPause() {
    super.onPause();
    saveState();
  }

  private void saveState() {
    String designation = (String) mDesignation.getSelectedItem();
    String province = (String) mProvince.getSelectedItem();
    String firstName = mFirstNameText.getText().toString();
    String lastName = mLastNameText.getText().toString();
    String address = mAddressText.getText().toString();
    String country = mCountryText.getText().toString();
    String postcode = mPostcodeText.getText().toString();
    
    // Only save if either summary or description
    // is available

    if (lastName.length() == 0 && firstName.length() == 0) {
      return;
    }

    ContentValues values = new ContentValues();
    values.put(MyAddressTableHandler.COLUMN_DESIGNATION, province);
    values.put(MyAddressTableHandler.COLUMN_FIRSTNAME, firstName);
    values.put(MyAddressTableHandler.COLUMN_LASTNAME, lastName);
    values.put(MyAddressTableHandler.COLUMN_ADDRESS, address);
    values.put(MyAddressTableHandler.COLUMN_PROVINE,province);
    values.put(MyAddressTableHandler.COLUMN_COUNTRY,country);
    values.put(MyAddressTableHandler.COLUMN_POSTCODE,postcode);

    if (addressUri == null) {
      // New address
      addressUri = getContentResolver().insert(MyAddressContentProvider.CONTENT_URI, values);
    } else {
      // Update address
      getContentResolver().update(addressUri, values, null, null);
    }
  }

  private void makeToast() {
    Toast.makeText(MyAddressDetailActivity.this, "Please maintain a summary",Toast.LENGTH_LONG).show();
  }
} 

