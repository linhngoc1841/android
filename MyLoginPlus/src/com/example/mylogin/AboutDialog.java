package com.example.mylogin;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class AboutDialog extends DialogFragment implements View.OnClickListener {
	
	public static AboutDialog
	
	newInstance()	{
		AboutDialog hdf = new AboutDialog();
		return hdf;
	}

    @Override    
    public void onCreate(Bundle icicle) {
    	super.onCreate(icicle);
    	this.setCancelable(true);
        int style = DialogFragment.STYLE_NO_TITLE, theme = 0;
        setStyle(style,theme);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle)  {
        View v = inflater.inflate(R.layout.about, container, false);
        return v;
    }
    
    public void onClick(View v) {
		dismiss();
	}
}
