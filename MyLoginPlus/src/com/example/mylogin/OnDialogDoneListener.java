package com.example.mylogin;

import android.app.DialogFragment;

public interface OnDialogDoneListener {
	public void onDialogDone(String tag, boolean cancelled, CharSequence message);

}
